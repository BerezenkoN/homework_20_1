﻿#main_content{
    background: url('https://img-fotki.yandex.ru/get/5908/43911121.179/0_96fc2_37f26a71_L') repeat;
    margin:auto;
    padding: 20px;
    width: 900px;
}
.right_column{
    float: right;
    width: 160px;
}
.left_column{
    margin-right: 170px;
}
#nav_container{
    list-style-type: none;
    background-color: #21653f;
    margin-bottom: 15px;
}
#nav_container> li{
    display: inline-block;
    text-decoration: none;
}
#nav_container> li > a{
    display: inline-block;
    padding: 5px;
    color: #2da282;
}
ul#nav_container> li > a:hover{
    background-color: #88d7ab;
}
